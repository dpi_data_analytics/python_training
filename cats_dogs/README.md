# CNN networks  
Important machine learning model for visualisation.  

The way that it works is that it works with 1s and 0s, by looking at pixels, creates 2d or 3d arrays depending on if the image is black and white or colour image, so you can work out red green and blue channels based on the encoding. 

If we have a smilly face we can use a 2x2 grid to say 0 is white and 1 is black and then encode the smilly face pixels as a matrix of 0 and 1s, depending on if there is white space or a black dot as part of the smily face. 

The steps of the CNN are:  
1. Convolution  
2. Max pooling  
3. Flattening  
4. Full connection  

## Step 1A convolution  
A combined integration of two functions, one function modifies the shape of the other. 

You use a 3x3 block, each of the 9 positions in this block (aka filter) has a number either 0 or 1, you then look to see if any of the numbers overlap when you overlay this filter over the image matrix (also 0s and 1s) then you score that as 1 if the filter position overlaps with a position in the image matrix, so the total result will be either 0 (no overlap between numbers in filter and the numbers in the image) or a number greater than 1 depending on how much overlap there is. In this way you turn a large matrix of 0s and 1s (ie the image matrix) into a much smaller matrix of summary results. The amount you move the filter each iteration is call the stride, most people use a stride of 2. This smaller matrix will be 0s, 1s, 2s etc depending on how many matches there were between the filter and the image at each step. 

This smaller matrix is called a `feature map`, this is smaller than the original image (important), if you have a large stride it will make the feature map even smaller, so this convolution step helps reduce the amount of data that the model needs to process. We do loose some information, but really the feature detector pattern is really the key info that we need, thus the higher the number in our feature map indicates that we had a better match between our image and the filter, so we are extracting features from the image based on the pattern in the filter. We don't need to look at every pixel to identify an image, these filters create a feature map that summarise the key elements in each image. It also preserves the positional information between features, due to the stepping across the image.  

We create multiple feature maps using a bunch of different filters, some will work better than others, the network uses the filters that best extract the information from the image. So the filter can be seen as a feature detector, as these filters are extracting the key info, some will do better than others at getting good matches to the image, the network will decide which ones are best at extracting the key information based on the training.  

## Step 1B ReLU layer  
This is applying a rectifier function (ie the line along the x-axis, that then goes up at 45 degrees to 1. We apply the rectifier because we want to increase non-linearality in our network, breaks up linearilty, because images themselves are highly non-linear, transition between pixels is often non-linear, because there are borders and gradients between features. When we apply the filter we can sometimes create something linear when really it is not. Often this involves removing the back from the image to break up the lineararity.  

## Step 2 Max pooling  
The same image can look different depending on the way the photo was taken ie the background, the angle, we need the algorhym to identify the thing in the image despite all of these possible repressentations of the image. Pooling deals with this. 

The way max pooling works is that we take our feature map (from the filter results), take 2x2 box of pixels, find max value in that box and record only that value, move box to the right by a specific stride, do the same again, 2 is commonly used for the stride. We preserve the features, the max numbers repressent the best overlap, by pooling the features we are getting rid of a lot of poor matches, because we take the maximium value we are accounting for any distortion, ie some alterations of key features between images the pooling with result in the same result because the suboptimal values are ignored. This reduces the size yet again, making for more effecient processing. We also reduce the number of paramaters that go into our final layers of the network, and this has the important feature of removing information and thus helping to avoid over fitting, which would be a big deal as we need a general model that can identify an object irrespective of lighting conditions, or the focus, or the angle the image was taken, this creates a more robust model that is hopefully not overfitted.  

## Step 3 Flattening  
All flattening does is take our pooled feature map, then we flaten it into a column, with row by row, this column is then an imput layer for the ANN. You may have many pooled feature layers given you would have used multiple filters, so these are lined up and made into a huge vector of inputs that is a column.  

## Step 4 full connection  
Add full ANN onto the back on the convolution. The fully connected layer are hidden layers, but these hidden layers are fully connected, in ANN they don't have to be fully connected. The vector of outputs after flattening are passed into the input layer. The main purpose is to combine the features into attributes that provide the best prediction of the image. The ANN deals with features and finds the best attributes to provide better predictions of the image.  

When you are doing classification you have one output per class (unless you have a binary output), but for more than two categories you would have a output neuron per category.  

Info moves from the input layer through the linked hidden layers to the output layers. The probability of a dog or cat is then produced, if this is an error we calculate the loss function that tells us how well the network is performing, this information is back propergated through the network and weights are adjusted to try and optimise the result. The feature detectors are also adjusted, in otherwords the filters are adjusted so that the filters that capture the most information are the most important ones for making predictions of our image. 

How do the output neurons work? If the top neuron is 'Dog', what weights do we assign to the synapsus linking up the dog, each neuron is just looking at a feature, some features are more confidently identifying the image, so they get different scores, these scores get passed to both output neurons. The output neurons know what the results are because they know the result, so when the confident value is passed to the dog they learn that this neruon is a good one for identifying the dog, likewise the cat neuron knows that if this neuron fires up then this means the result is a dog. Based on this, the synapeses are given more wieghting in the decision. When it is a cat, this time a different combination of neurons will be important for identifying what is a cat. Both the dog and the cat can see what the image is, so they just make use of neurons that provide information that differentiates between the images.  

When the CNN is actully used with the real data they don't know the result, however they have learned to recognise what combination of neurons provide the most infomration on thei mages, so they are able to make predictions about how confident they are that the image is cat or dog.  

## Softmax and cross entropy  
The softmax function creates the probabilities for answer 1 versus answer 2, this is because the two output nourons can't see each others result, so this would mean the probabilities would not add up to 1. The softmax takes these two final output values and creates a probability of a + b = 1.

The cross entropy function is somewhat related, this acts as a loss function (like a cost function) for CNN, the loss function is minimised to maximse the performance of the network. If during training we have a dog image, the model predicts 90 for dog and 10 for cat, these values are plugged into the cross entry equation, when we try to minimise the loss function as a way of picking the best neural network. The reason cross entropy is better than mean squared error or other loss functions, because it is robust to pertibations and not subject to outlier effects, this is because of the logorhym functions acts to moderate these extreams avoiding local optima.

## Summary  
See `https://adeshpande3.github.io/The-9-Deep-Learning-Papers-You-Need-To-Know-About.html`.


