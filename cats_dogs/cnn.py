# Convolutional Neural Network

# Installing Keras
# Enter the following command in a terminal (or anaconda prompt for Windows users): conda install -c conda-forge keras
# predict image of a cat or a dog.
# see dataset folders showing images of cats and dogs, could change these
# images to what every you want.

# In this case the datasets folder
# We no longer have rows observations and columns independ vars, instead we
# have pixes in images, so what do we use as a dependent varibale column? How
# do we extract the info of the dependent var. To overcome this we put the
# training images in different directories, then name these images after the
# category. So the training set for dogs contains the following
#dataset/training_set/dogs/dog.1.jpg
#dataset/training_set/dogs/dog.2.jpg
# However, keras has helper tools that allow us to import images as long as we
# use a specific structure.

## to use keras
#1 separate images into a training set and test set folder
#2 Make a folder for cat and one folder for dog in these training and test set folders.
# Keras uses these folder names to assign a categorical variable.

# Part 1 - Building the CNN
# In this example we have 10K in total, 
# 4K of dogs and 4K of cats in training set (total 8K) 
# And 1000 dogs and 1000 cats in the test set (2K) total

# Importing the Keras libraries and packages
# use initialse the neural network, sequence of layers or graph
from keras.models import Sequential
# package that convolutional layers with 2D images
from keras.layers import Conv2D
# max pooling of 2D images
from keras.layers import MaxPooling2D
# step 3 flattening the pooled feature maps into a large feature vector
from keras.layers import Flatten
# add the fully connected layers in a classic conected CNN
from keras.layers import Dense

# Initialising the CNN
classifier = Sequential()

# Step 1 - Convolution, call this classifyier as we are using cats and dogs
# add layers, convolution layer is first, this uses the Conv2D function, which
# takes as arguments: 32 is the number of filters (feature detectors), this
# will create 32 filter maps, each filter will be 3x3, 32 is a comon starting
# point with an increase to 64 and then 128 etc.
# input shape is shape of input image on which you will apply feature detecotrs
# as our images are all different shapes this forces them to all be hte same
# This forces them all into fixed size 
# color will be 3D array, black and white would be 2D array. As these are color
# images we have 3 channels (one if black and white), then 64 pixels * 64
# pixcels to reduce the processing time.
### WARNING #### 
# tensor flow back end 3D array dims, then the number of channels.
# if you are using another backend then it would be 3, 64,64)
# the activation function relu to avoid negative pixel values in feature map to
# avoid co-linearilty in our model
classifier.add(Conv2D(32, (3, 3), 
                      input_shape = (64, 64, 3), 
                      activation = 'relu'))

# Step 2 - Pooling
# reducing size of feature maps, take 2x2 table and take maximum of the four
# cells in the square, we stride with a stride of 2, this is a common setting
# this will harve the size of the feature maps
classifier.add(MaxPooling2D(pool_size = (2, 2)))

# Adding a second convolutional layer
classifier.add(Conv2D(32, (3, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))

# Step 3 - Flattening
# this simple vector will be a layer
# we don't loose our spatial structure using the flattening, because the order
# of the filters repressent the spatial information in these images. As the we
# have extracted info using filters, then each node repressents a key feature
# in the images. If we did not do this then each node would be a simple pixel
# that has very little info.
classifier.add(Flatten())

# Step 4 - Full connection
# dense is a function to add a fully connected layer, the number of hidden nodes is
# 128, but this could be optimised
# We need the activation function to pass on their signal use relu in the
# hidden layer
classifier.add(Dense(units = 128, activation = 'relu'))
# this is the output layer, only 1 node because this is a binary outcome, if
# there were more than 2 classes we would have one per class, and we would use
# softmax activation
classifier.add(Dense(units = 1, activation = 'sigmoid'))

# Compiling the CNN
# stocastic gradient decent is 'adam'
# binary crossentrpy good for classification problems with binary outcome
# if had more than two categories, would choose categorical_crossentry
# Metrix is the performance metric, accuracy is the most common one.
classifier.compile(optimizer = 'adam', 
                   loss = 'binary_crossentropy', 
                   metrics = ['accuracy'])

# Part 2 - Fitting the CNN to the images

from keras.preprocessing.image import ImageDataGenerator
# this is an important step, it pre-processes the image to avoid too much over
# fitting, so the models are more flexible.
# see pre-processing options at ://keras.io/ 
# see https://keras.io/preprocessing/image/#imagedatagenerator-class
# we only have 8K images, so this data augmentation will create batches of
# images which have some transformations to them, like rotation, flipping, or
# even shearing them, to get a much more diverse set of images to train on.
# rescale is the feature transformation 
train_datagen = ImageDataGenerator(rescale = 1./255,
                                   shear_range = 0.2,
                                   zoom_range = 0.2,
                                   horizontal_flip = True)

# we need to rescale again for feature transformation
test_datagen = ImageDataGenerator(rescale = 1./255)
# this expects the directory structure that we have in this example
# the target size is 64x64 which is what we set above as well
# batch size random samples of images that will go through before weiths are
# updated
# class mode is binary or more than two cateogries
training_set = train_datagen.flow_from_directory('dataset/training_set',
                                                 target_size = (64, 64),
                                                 batch_size = 32,
                                                 class_mode = 'binary')
# as before
test_set = test_datagen.flow_from_directory('dataset/test_set',
                                            target_size = (64, 64),
                                            batch_size = 32,
                                            class_mode = 'binary')
# apply our model and also test it
# number of images is the number of, so each epoch takes one image at a time, we want to
# go through all images, so we need 8000 images to move through each epoch
classifier.fit_generator(training_set,
                         steps_per_epoch = 8000,
                         epochs = 25,
                         validation_data = test_set,
                         validation_steps = 2000)

#  acc: 0.9975 - val_loss: 1.6358 - val_acc: 0.8110
# this was the final line, so the training clearly was over trainied as we got
# an accurcy of 99%, whilte the validation was 81%, so we probably should add
# more convultion layers or increase the image size from 64x64  to get more
# information moving through the network.
