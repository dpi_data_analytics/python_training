# 2018 Python training course (test run)  

This repo contains the materials for the DPI (NSW) python training course.  

The following branches are available:  


## Get python and the IDLE  
Pyton is free open source software. The package comes with a simple development
environment (IDE) called IDLE.  

Windows: https://www.python.org/ftp/python/3.7.0/python-3.7.0-amd64.exe  
Mac: https://www.python.org/ftp/python/3.7.0/python-3.7.0-macosx10.9.pkg  

## What is in this repo  
1.  Instructions.pdf - Instructions for checking your python install and making
    use of this repo  
2.  dropbear.tab.txt - a spreadsheet of dropbair observations  
3.  day1.ipynb - Notebook of python training notes from day 1  
4.  day2.ipynb - Notebook of python training notes from day 2  

## Notes  
Various bits and bobs  
