from math import radians, cos, sin, asin, sqrt

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles
    return c * r

## 1. create a list called 'orange_long_lat' hold the Orange cords ie
## lat=-33.2835, long=149.0996
orange_long_lat = [149.0996, -33.2835]

## 2. set a variable called 'target_dist' 
## hat holds an int which is the distance from 
## orange to our target dropbears ie 50 (50 kms)


## 3. create an empty list called 'found' to 
## hold dropbear IDs of interest aka 
## lon/lat < target distance
## the list will hold a sub-list [dropbear_id, distance]
## that we will extract from rows where distance < 50

## 4. loop through data file
# with open(file) as f:
#    for line in f:
        # make the row (a string) into a list using split('\t')
        # do soemthing with the columns in this row
        # col 1 (remember python counts from 0) is dropbear id
        # col 4 is lat as a string! use float(val)
        # col 5 is long as a string!
        # assign the results of the haversine to a variable called distance
        # 6. test the result aginst our target using:
        # if distance < target_dist:
            # do something
            #ie extract dropbear ID and assign it to a variable called
            # 'dropbear_id'
            # Add the dropbear ID and distance info to a list
            # data = [dropbear_id, distance]
            # and add this to the 'found' list
            #found.append(data)
        # else:
        # do nothing if distance > 50 with the pass keyword
        #   pass

## 9. open a new file for writting called 'orange_dropbears.txt'
# with open(outfile, 'w') as w:
    # for data in found:
        # distance = data[...] # insert index here
        # dropbear_id = data[...] # insert index here
        # use "%s\t%s\n"%(val1, val2) to write this to the file
