#!/usr/bin/env python

import random
import numpy as np

# build a table of Drop bear observations, use randomness to make the table
# funcky for learning about python
# funckyness = D instead of DB for IDs
# ? for sex
# +ve latit values

# create some data cats
sex_opt = ['M', 'F']

# excel <2013 max rows are 65536
IDrange = range(1,70001)

# Python range only works with ints
infra_opt = np.linspace(0.1, 10, 1000) # 1000 values between 0.1 and 10

# Australia bounds roughly
long_opt = np.linspace(114, 153, 10000) # 10000 values
lat_opt = np.linspace(-38, -12, 10000)

orange_cord = [-33.3283107, 149.0529105]

# make the table
heading = ['DropBearID', 'InfraRed', 'Sex', 'long', 'lat']

counter = 0

with open('dropbear.tab.txt', 'w') as wf:
    header = '\t'.join(heading) + '\n'
    wf.write(header)
    for number in IDrange:
        drop_id = 'DB_' + str(number)
        infared = random.choice(infra_opt)
        lon = random.choice(long_opt)
        lat = random.choice(lat_opt)
        sex = random.choice(sex_opt)
        row = "%s\t%s\t%s\t%s\t%s\n" % (drop_id, infared,
                                        sex, lon, lat)
        wf.write(row)
